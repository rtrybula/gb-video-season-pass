if (!Array.prototype.some)
{
	Array.prototype.some = function(fun /*, thisArg */)
	{
	  'use strict';

	  if (this === void 0 || this === null)
	    throw new TypeError();

	  var t = Object(this);
	  var len = t.length >>> 0;
	  if (typeof fun !== 'function')
	    throw new TypeError();

	  var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
	  for (var i = 0; i < len; i++)
	  {
	    if (i in t && fun.call(thisArg, t[i], i, t))
	      return true;
	  }

	  return false;
	};
}

var SeasonPass = function(){};
SeasonPass.prototype = {
		VERSION: '1.63',
		REQUEST_URL: 'http://gbvideoguide.herokuapp.com/Video/GetParts',
		SHOWS_REQUEST_URL: 'http://gbvideoguide.herokuapp.com/Shows/',
		UPDATE_URL: 'http://gbvideoguide.herokuapp.com/CheckForUpdate',
		VIDEO_PAGE_TAG: 'div[itemprop="video"]',
		VIDEO_TITLE_TAG: 'h2[itemprop="name"]',
		VIDEO_PLAYER_TAG: 'div.av-wrapper-max',
		VIDEO_PLAY_TAG: 'div.js-vid-play',
		ME_TAG: 'div.seasonpass-box',
		AV_CONTROL_TAG: 'li.seasonpass-guide-btn',

		titlePartTester: VerEx().find("Part").then(" ").maybe(VerEx().word()).maybe(VerEx().word()).maybe(VerEx().then("-")).range(0,9).maybe(VerEx().range(0,9)),
		titleDayTester: VerEx().find("Day").then(" ").range(0,9).maybe(VerEx().range(0,9)),

		util: {
			escapeQuotes: function(string) {
				return string.replace(/"'"/g, "");
			}
		},

		init: function() {
			if(this.isVideoPage()) {
				var callback = function() {
				 	if(this.showTester() || this.hasParts())
						this.getEpisodes(this.attachToVideoPlayer);
				}

				this.getShows.call(this, callback);
			}

		},

		isVideoPage: function() {
			var videoTag = $(this.VIDEO_PAGE_TAG);
			if(videoTag.length > 0) {
				return true;
			}
			else
				return false;
		},

		getShows: function(callback) {
			var me = this;

			//keep a local array for now until timeout errors on heroku are figured out
			$.ajax(me.SHOWS_REQUEST_URL, {
				method: 'GET',
				timeout: 3000,
				complete: function(response) {
					if(response.responseJSON)
						me.SHOWS = response.responseJSON;

					callback.call(me);
				}
			});
		},

		hasParts: function() {
			if(this.titlePartTester.test(this.getVideoTitle()) || this.titleDayTester.test(this.getVideoTitle())) {
				return true;
			}
			else
				return false;
		},

		showTester: function() {
			var me = this;

			if(me.SHOWS) {
				me.SHOWS.some(function(item){
					if ( (new RegExp('\\b'+item+'\\b')).test(me.getVideoTitle()) ) {
						me.isShow = true;
						me.showQuery = item;
						//return true;
					}
					// else
					// 	return false;
				});
			}

			if(me.isShow) {
				return true;
			}
			else
				return false;
		},

		getVideoTitle: function() {
			if(!this.videoTitle)
				this.videoTitle = $(this.VIDEO_TITLE_TAG)[0].textContent;

			return this.videoTitle;
		},

		getVideoURL: function() {
			if(!this.videoURL)
				this.videoURL = $('meta[property="og:url"]')[0].content;

			return this.videoURL;
		},

		getVideoId: function() {
			if(!this.videoId)
				this.videoId = this.getVideoURL().split('/')[5].split('-')[1];

			return this.videoId;
		},

		getEpisodes: function(callback) {
			var me = this;

			$.ajax(me.REQUEST_URL, {
				method: 'POST',
				data: {
					videoTitle: this.generateSearchQuery(this.getVideoTitle())
				},
				complete: function(response) {
					//if(err)
					//	me.onGetEpisodesFailure.call(me, err);
					//else
						me.onGetEpisodesSuccess.call(me, jQuery.parseJSON(response.responseJSON.body), callback);
				}
			});
		},

		onGetEpisodesSuccess: function(response, callback) {
			var me = this;
			var videos = [];

			//find video parts and attach as prop in video.parts
			for(var i = 0; i < response.results.length; i++) {
				var video = response.results[i];

				if(me.isShow) {
					//let's double check to make sure we didn't receive a video that's just similarily named
					//the video name should start with the show name
					var exp = new RegExp('^' + me.showQuery);
					var match = response.results[i].name.match(exp);

					//if it matches, let's add it
					if(match) {
						me.applyPartsToVideo(video);
						videos.push(video);
					}
				}
				else {
					me.applyPartsToVideo(video);
					videos.push(video);
				}
			}

			//sort by publish date
			videos.sort(function(a,b){

				//split dates up into their parts since publish_date has hyphens, which firefox/safari does not like
				var dateA      = a.publish_date.split(' ');
				var dateA_YMD  = dateA[0].split('-');
				var dateA_Time = dateA[1].split(':');

				var dateB      = b.publish_date.split(' ');
				var dateB_YMD  = dateB[0].split('-');
				var dateB_Time = dateB[1].split(':');

				//Year, Month, Day, Hour, Minute, Second
				return new Date(dateA_YMD[0], dateA_YMD[1], dateA_YMD[2], dateA_Time[0], dateA_Time[1], dateA_Time[2]) - new Date(dateB_YMD[0], dateB_YMD[1], dateB_YMD[2], dateB_Time[0], dateB_Time[1], dateB_Time[2]) ;
			});

			//data for tpl
			var guideData = {
				videos: videos,
				formatMultiPart: function() {
					return function(first_part, second_part, render) {
						if(!me.lastValue) {
							me.lastValue = first_part;
							return render(first_part);
						}
						else if(me.lastValue != first_part)
							return render(first_part);
					}
				}
			};

			var template =
			'<div class="seasonpass-box animated fadeInRight">' +
			'<h3>Episodes</h3>' +
				'{{#videos}}' +
				   '<div class="seasonpass-video-wrapper"  videoId="{{ id }}">' +
				    '<a href="{{ site_detail_url }}">' +
					'<div class="episode-name">{{ episodeName }}</div>' +
					'<li class="seasonpass-video" title="{{ name }}">' +
						'<div class="episode-info"><img src="{{ image.icon_url }}"><span class="part">{{ parts.first_part.number }}{{#parts.second_part.number}}.{{/parts.second_part.number}}{{ parts.second_part.number }}</span></img><span class="status">Playing</span></div><p class="deck">{{ deck }}</p>' +
					'</li></a></div>' +
				'{{/videos}}' +
			'</div>';

			var html = Mustache.render(template, guideData);

			callback.apply(me, [html]);
		},

		onGetEpisodesFailure: function(err) {
			console.error(err);
		},

		applyPartsToVideo: function(video) {
				var parts = this.stripVideoTitleToParts(video.name);
				video.parts = {};

				//if it's not a show, we'll look thru and try to parse the parts of the name
				if(!this.isShow) {
					var firstPart = parts[0];

					if(firstPart) {
						if(parts.length > 1)
							var secondPart = parts[1];

						if(firstPart.split(' ').length > 1)
							if(firstPart.split(' ')[0] == 'Day')
								video.hasDay = true;

						video.parts.first_part = {
							label: firstPart.split(' ')[0],
							number: firstPart.split(' ')[1]
						};

						if(parts.length > 1) {
							video.hasMultipleParts = true;
							video.parts.second_part = {
								label: secondPart.split(' ')[0],
								number: secondPart.split(' ')[1]
							}
						}
					}
					else {
						//if it's more of an anomaly in naming convention, let's just display the full name
						video.episodeName = video.name;
					}
				}
				else {
					if(parts[1]) {
						//remove any chars at the beginning that may be inconsistent
						if(parts[1].charAt(0) == ':')
							video.episodeName = parts[1].replace(':', '').trim();
						else if(parts[1].charAt(1) == '-')
							video.episodeName = parts[1].replace('-', '').trim();
						else
							video.episodeName = parts[1].trim();
					}
				}
		},

		generateSearchQuery: function(title) {
			if(this.isShow)
				return this.showQuery;
			else {
				var exp = /^(.*?)(Day|Part)/;
				return title.match(exp)[0];
			}
		},

		setWatching: function(title) {
			$('div.seasonpass-video-wrapper[videoId="' + this.getVideoId() + '"]').addClass('watching');
		},

		stripVideoTitleToParts: function(title) {
			var exp;
			var me = this;

			if(!me.isShow)
				exp = /(Day|Part).(\d+)/g;
			else
				exp = new RegExp(me.showQuery + "(.*)");

			var match = title.match(exp);
			if(match == null) {
				//check for an unconventional naming of part
				exp = /(Day|Part).(\w+)-(\d+)/g;
				match = title.match(exp);

				//check for spelling instead of actual numbers
				if(match == null) {
					exp = /(Day|Part).(\w+)/g;
					match = title.match(exp);
				}
			}

			if(match)
				return match;
			else
				return "";
		},

		scrollToEpisode: function() {
			var me = this;
      		var scrollFunc = function() {
				$(me.ME_TAG).animate({
						scrollTop: $("div.seasonpass-video-wrapper[videoId='" + me.getVideoId() + "']").offset().top - 250
				}, 0);
			}

			var hideGuide = function() {
					me.toggleGuide.apply(me);
			};


      		window.setTimeout(scrollFunc, 0);
			//window.setTimeout(hideGuide, 3000);
		},

		attachGuideEventHandlers: function() {
			var guide = $(this.ME_TAG);
			var onAnimationEnd = function(e) {
				//guide-hidden changes display to none. we need to add it at the end of the animation so it doesn't interfere
				if(guide.css('opacity') == 0)
					guide.addClass('guide-hidden');
			};
			guide.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', onAnimationEnd);
		},

		toggleGuide: function() {
			var guide = $(this.ME_TAG);

			//if it has guide-hidden now, it means we're showing the guide, so the button text should read the opposite
			if(guide.hasClass('guide-hidden'))
				$(this.AV_CONTROL_TAG).text('Hide Episodes');
			else
				$(this.AV_CONTROL_TAG).text('Show Episodes');

			guide.removeClass('guide-hidden');

			guide.toggleClass('fadeInRight');
			guide.toggleClass('fadeOutRight');
		},

		//if a user clicks to start a video, hide the episode guide
		setPlayListener: function() {
			var me = this;
			$(me.VIDEO_PLAY_TAG).click(function() {
				if(!$(me.ME_TAG).hasClass('guide-hidden'))
					me.toggleGuide();
			});
		},

		attachToVideoPlayer: function(dialogHtml) {
			var me = this;
			$('ul.av-options').append('<li class="av-menu-hit av-gear seasonpass-guide-btn">Hide Episodes</li>');

			$(me.AV_CONTROL_TAG).click(function() {
				me.toggleGuide.apply(me);
			});

			$(this.VIDEO_PLAYER_TAG).append(dialogHtml);

			me.attachGuideEventHandlers();
			this.setWatching();
			this.scrollToEpisode();
			this.setPlayListener();
		}
};

var seasonPass = new SeasonPass();
seasonPass.init();
